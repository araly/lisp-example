;;;; hello-world.lisp

(in-package #:hello-world)

(defun main ()
  "main entry"
  (format t "hello world~%"))

