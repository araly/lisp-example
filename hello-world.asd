;;;; hello-world.asd

(asdf:defsystem #:hello-world
  :description "Describe hello-world here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "hello-world"))
  :build-operation "program-op" ;; leave as is
  :build-pathname "hello-world"
  :entry-point "hello-world:main")
