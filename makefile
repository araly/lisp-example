LISP ?= sbcl

build:
	$(LISP) --load hello-world.asd \
    	--eval '(ql:quickload :hello-world)' \
		--eval '(asdf:make :hello-world)' \
		--eval '(quit)'
